# Bowling Scoreboard

This is a .NET solution to the challenge of making a system for bowling game scoring. The solution consists of three projects:

- **BowlingScoreboardBusinessLayer**: Contains the main BowlingScoreboard class and other sub-components. This is where the game and scoring logic resides. 
- **BowlingScoreboardBusinessLayerTetst**: Simple test-layer with unit tests asserting correct scores in different game scenarios.
- **BowlingScoreboardCLI**: Sample user-facing presentation layer using a command line interface and "beautiful" ASCII tables.

# Getting Started
To run the CLI sample application, build it and run `BowlingScoreboardCLI.exe` from the output-location.

# Design choices
When I started looking to build this system, I sat down and tried to identify the components that would make up a bowling scoring system. 
I identified the following main components from which I would design my system:
- The scoreboard: This is the main unit which consumers of the business logic should interact with. 
- Players/Individual Player Score: In any bowling game there is at least one player, and most likely more. The scoreboard needs to track the score of each player individually.
- Frames: In the bowling rules presented in the challenge specification, 10 frames are played in a game. Each player plays 10 frames.

![Bowling scoreboard components](https://i.imgur.com/v60W9jq.png)

First I started thinking about the strategy pattern and creating interfaces to inject into the Scoreboard with different responsibilities, for instance, Turn handling and Scoring, in order to be able to handle multiple types of bowling rules. However, since this was a part of the specification, I tried to adhere to the principles of extreme programming and limit myself to only write code that solves the actual problem, instead of trying to solve those that have not come up yet - and possibly will not.

So I went for a more simple approach with three classes: `BowlingScoreboard`, `PlayerScoreLine` and `Frame`, in order to capture the components I identified and illustrated above.

In describing the components, I'm gonna start bottom-up.

## Frame
`Frame` is a class that encapsulates the results of a player's performance in a single frame. It contains the player's rolls (up to three) and a link to the next frame. I added the reference to the next frame in order to more easily calculate the bonus score of a frame, according to the rules of strikes and spares. 

I considered doing the scoring by adding knocked pins to previous frames (look-behind), but this would require that the state of previous frames were updated. As I wanted to update as little state as possibly for each roll, for less complexity, I only wanted to update the state of the current frame.

Thus, I opted for solution where the score is calculated only when needed. When the calculation for a frame is performed, it looks to the next frame if it is eligible for bonus points.

The scoring logic resides in the `Frame` class, since this is where I want to query for the score. I want to be able to take a Frame and ask for its score. I considered using class specific to scoring, which given a frame could calculate the scores needed. This is definitely a better way to go if you want to be able to configure the scoring algorithm (multiple strategies).

Besides the scoring logic and the data-state in the Frame class, it also contains some helper-properties to use for finding out whether a frame contains a strike or a spare, is a gutter ball or is closed for more rolls. This is both helpful in the business logic, but also in the application layer when needing to draw particular UI for different states.

## PlayerScoreLine
Players do not exist as seperate objects but as part of a `PlayerScoreLine`, which contains all the frames that player has played. The bowling scoreboard keeps track of a list of these score lines. In this iteration of the system, a player is simply represented as a string. This makes for a simple solution which solves the problem. I added the constraint that you cannot add two players with the same name, which I deemed reasonable considering that players have to be able to identify themselves when they look at the scoreboard.

You could use a more complex data object with an identifier, a player name, etc., which would make sense if you were required to, for instance, keep track of a certain player's game history. However, this is not the case here.

When interacting with a `PlayerScoreLine`, you can add a new frame, add a roll to the current frame or get the total score of the player. 

I considered not using Player Score Lines, and instead keep a single list of `Frame`-objects which would then each contain a dictionary mapping from a player identifier to a player score (containing the scores, player rolls, etc.). While this would allow for the same functionality, I find that it is less close to the actual real-world domain -- visualized by my illustration in the beginning of this section.

## BowlingScoreboard
The bowling scoreboard contains a list of `PlayerScoreLine`, one for each player, and keeps track of whose turn it is. Simply put, it is responsible for handling the course of the game. In order to be able to handle this responsibility, it exposes 4 main methods for interaction:
- `AddPlayer`
- `AddRoll`
- `GetPlayerScore`
- `GetCurrentPlayerScore`

These methods are enough to handle a game. However, in a later iteration of this system, it would be ideal if it was possible to also remove a player, edit a score and possibly more.

`AddPlayer` and `AddRoll` can be used by consumers of the `BowlingScoreboard` to move the game forward, while `GetPlayerScore` and `GetCurrentPlayerScore` can be used to check on the game's current state. 

The first two mentioned both have no return value, for instance describing the new game state. Instead, I opted for an event-driven approach with three events `NextRoll`, `GameScoreUpdated` and `EndGame` that an application using the bowling scoreboard can subscribe to and use to trigger UI drawing and querying the user for the next action.

This can be seen in `Program`-class in the BowlingScoreboardCLI-project, the program respons to these three events to drive the game forward.

In this way, multiple applications/services can subscribe to the scoreboard system - for instance a UI application on a screen and a logging-service at the same time. Of course, in a distributed systems environment, one would have to be a little more sophisticated than using the built-in C# events - however, the basic principle is the same.

# Bowling Scoreboard CLI
I created a sample CLI program found in `Program.cs`, which allows the user to add some players, start a game and add some rolls. After each roll, the scoreboard is drawn using an ASCII-table which took more time to make than I care to admit. Note, that this is not the primary work of my solution to this challenge, but it serves as a nice way to manually test the system.

# Authors
- Mathias Skovgaard Birk

# License
MIT License

Copyright (c) [year] [fullname]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
