﻿using System;
using System.Collections.Generic;
using System.Linq;
using BowlingScoreboardBusinessLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BowlingScoreboardBusinessLayerTest
{
    [TestClass]
    public class TenPinBowlingScoreboardTest
    {
        private List<string> _players;
        private BowlingScoreboard _scoreboard;

        [TestInitialize]
        public void InitializeDefaultGame()
        {
            _players = new List<string> { "Player 1", "Player 2" };
            _scoreboard = new BowlingScoreboard();
            foreach(string player in _players)
            {
                _scoreboard.AddPlayer(player);
            }
        }
        
        [TestMethod]
        public void ShouldInitializeEmptyGameAndAddPlayers()
        {
            var scoreBoard = new BowlingScoreboard();
            var players = new List<string>() { "Mathias", "Henrik" };
            scoreBoard.AddPlayer(players[0]);
            scoreBoard.AddPlayer(players[1]);

            Assert.AreEqual(2, players.Count);
            Assert.AreEqual("Henrik", scoreBoard.ScoreLines[1].PlayerName);
        }

        [TestMethod]
        public void ShouldAddPlayerAfterGameBegun()
        {
            var scoreBoard = new BowlingScoreboard();
            var players = new List<string>() { "Mathias", "Henrik" };
            scoreBoard.AddPlayer(players[0]);
            scoreBoard.AddPlayer(players[1]);
            //Mathias' turn:
            scoreBoard.AddRoll(10);

            //Henrik's turn:
            scoreBoard.AddRoll(3);
            scoreBoard.AddRoll(6);

            //Mathias' turn:
            scoreBoard.AddPlayer("Player 3");
            scoreBoard.AddRoll(2);
            scoreBoard.AddRoll(4);

            //Henrik's turn:
            scoreBoard.AddRoll(3);
            scoreBoard.AddRoll(6);

            //Player 3's turn:
            scoreBoard.AddRoll(10);

            Assert.AreEqual(22, scoreBoard.GetPlayerScore("Mathias").TotalScore);
            Assert.AreEqual(18, scoreBoard.GetPlayerScore("Henrik").TotalScore);
            Assert.AreEqual(10, scoreBoard.GetPlayerScore("Player 3").TotalScore);
        }

        [TestMethod]
        public void ShouldPlaySimpleFrameWithoutBonus()
        {
            var currentPlayer = _players[0];
            _scoreboard.AddRoll(3);
            var currentScore = _scoreboard.GetPlayerScore(currentPlayer).Frames.Last();
            Assert.AreEqual(3, currentScore.RegularScore);
            Assert.AreEqual(0, currentScore.BonusScore);
            Assert.AreEqual(3, currentScore.TotalScore);
            _scoreboard.AddRoll(6);
            Assert.AreEqual(9, currentScore.RegularScore);
            Assert.AreEqual(0, currentScore.BonusScore);
            Assert.AreEqual(9, currentScore.TotalScore);
        }

        [TestMethod]
        public void ShouldPlayAllFramesWithoutBonus()
        {
            var currentPlayer = _players[0];
            var lastRoll = 9;

            _scoreboard.NextRoll += (sender, eventArg) =>
            {
                currentPlayer = eventArg.PlayerToRoll;
                lastRoll = Math.Abs(lastRoll - 9);
                _scoreboard.AddRoll(lastRoll);
            };

            _scoreboard.EndGame += (sender, eventArg) =>
            {
                var scoreLines = eventArg.ScoreLines;
                var totalScore = scoreLines[0].TotalScore;
                Assert.AreEqual(90, totalScore);
            };

            _scoreboard.AddRoll(lastRoll);
        }

        [TestMethod]
        public void ShouldPlayFrameWithSpareBonus()
        {
            var currentPlayer = _players[0];

            _scoreboard.NextRoll += (sender, eventArg) =>
            {
                currentPlayer = eventArg.PlayerToRoll;
                _scoreboard.AddRoll(5);
            };

            _scoreboard.EndGame += (sender, eventArg) =>
            {
                var scoreLines = eventArg.ScoreLines;
                var totalScore = scoreLines[0].TotalScore;
                Assert.AreEqual(150, totalScore);
            };

            _scoreboard.AddRoll(5);
        }

        [TestMethod]
        public void ShouldPlayFrameWithStrikeBonus()
        {
            var currentPlayer = _players[0];

            _scoreboard.NextRoll += (sender, eventArg) =>
            {
                currentPlayer = eventArg.PlayerToRoll;
                _scoreboard.AddRoll(10);
            };

            _scoreboard.EndGame += (sender, eventArg) =>
            {
                var scoreLines = eventArg.ScoreLines;
                var totalScore = scoreLines[0].TotalScore;
                Assert.AreEqual(300, totalScore);
                Assert.AreEqual(300, scoreLines[1].TotalScore);
            };

            _scoreboard.AddRoll(10);
        }
    }
}
