﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BowlingScoreboardBusinessLayer.Models
{
    /// <summary>
    /// Players do not exist as seperate objects but as part of a `PlayerScoreLine`, which contains all the frames that player has played. 
    /// When interacting with a `PlayerScoreLine`, you can add a new frame, add a roll to the current frame or get the total score of the player. 
    /// </summary>
    public class PlayerScoreLine
    {
        public string PlayerName { get; }
        public List<Frame> Frames { get; }

        public PlayerScoreLine(string playerName)
        {
            PlayerName = playerName;
            Frames = new List<Frame>();
        }

        public int TotalScore => Frames.Sum(f => f.TotalScore);

        public void AddFrame(Frame frame)
        {
            if (Frames.Count == 10)
                throw new ArgumentOutOfRangeException("Only 10 frames allowed in one player score line");

            if (Frames.Any())
            {
                Frames.LastOrDefault().NextFrame = frame;
            }

            Frames.Add(frame);
        }

        public void AddRoll(int pinsKnocked)
        {            
            //Update latest frame:
            var latestFrame = Frames.Last();
            latestFrame.AddRoll(pinsKnocked);
        }
    }
}
