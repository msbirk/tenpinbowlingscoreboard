﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BowlingScoreboardBusinessLayer.Models
{
    public class Frame
    {
        public List<int> Rolls { get; }
        public Frame NextFrame { get; set; }

        public Frame(bool isLastFrame = false) {
            IsLastFrame = isLastFrame;
            Rolls = new List<int>();
        }

        public bool IsLastFrame { get; }
        public int TotalScore => CalculateScore();
        public int RegularScore => Rolls.Sum();
        public int BonusScore
        {
            get
            {
                if (IsStrike && NextFrame != null)
                {
                    //Return next frame's first roll + either the same frame's next roll, or if that is also a strike, the next-next frame's first roll.
                    return NextFrame.Rolls.ElementAtOrDefault(0) + (NextFrame.IsStrike && NextFrame.NextFrame != null ? NextFrame.NextFrame.Rolls.ElementAtOrDefault(0) : NextFrame.Rolls.ElementAtOrDefault(1));
                }
                else if (IsSpare && NextFrame != null)
                {
                    //Return first roll of next frame
                    return NextFrame.Rolls.ElementAtOrDefault(0);
                }

                return 0;
            }
        }

        public void AddRoll(int pinsKnocked)
        {
            if (!IsLastFrame && (Rolls.ElementAtOrDefault(0) + pinsKnocked) > 10)
            {
                throw new InvalidOperationException("Not possible to strike more than 10 pins in one regular round");
            }
            Rolls.Add(pinsKnocked);
        }

        public bool IsStrike
        {
            get
            {
                return Rolls.ElementAtOrDefault(0) == 10;
            }
        }

        public bool IsSpare
        {
            get
            {
                return !IsStrike && Rolls.Take(2).Sum() == 10;
            }
        }

        public bool IsGutter
        {
            get
            {
                return Rolls.Take(2).Sum() == 0;
            }
        }

        public bool IsClosed
        {
            get
            {
                //No more rolls allowed if:
                //- it is the last frame, 3 rolls have been made or only 2, but no bonus shot is needed.
                //- It is a regular frame, there is a strike, or two rolls have been made.
                if (IsLastFrame)
                {
                    return Rolls.Count() == 3 || (!IsStrike && !IsSpare && Rolls.Count() == 2);
                } else return Rolls.Count() == 2 || IsStrike;
            }
        }

        private int CalculateScore()
        {
            var allPinsKnocked = Rolls.Sum();
            if (IsLastFrame)
                //Is last Frame
                return allPinsKnocked;
            else
                return allPinsKnocked + BonusScore;
        }
    }
}
