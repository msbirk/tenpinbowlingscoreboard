﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BowlingScoreboardBusinessLayer.Models
{
    /// <summary>
    /// This is the main class for the Bowling Scoreboard, exposing a number of methods for interactions:
    /// - AddPlayer
    /// - GetPlayerScore
    /// - GetCurrentPlayerScore
    /// - AddRoll
    /// 
    /// This design is event-driven in the sense that one can hook up to the following three events, and get notified when it happens:
    /// - NextRoll
    /// - GameScoreUpdated
    /// - EndGame    
    /// This is useful when designin a consumer of the scoreboard processing - for instance a user-facing UI application.
    /// 
    /// The BowlingScoreboard
    /// </summary>
    public class BowlingScoreboard
    {
        public List<PlayerScoreLine> ScoreLines { get; }
        
        private int _turn;

        private const int NUMBER_OF_PINS = 10;
        private const int NUMBER_OF_FRAMES = 10;

        public BowlingScoreboard() {
            ScoreLines = new List<PlayerScoreLine>();
        }

        public void AddPlayer(string playerName)
        {
            if (ScoreLines.Select(sl => sl.PlayerName).Contains(playerName))
            {
                throw new InvalidOperationException("A player with that name already exists in the game.");
            }

            var newScoreLine = new PlayerScoreLine(playerName);
            if (ScoreLines.Any())
            {
                var numberOfFramesToAdd = GetCurrentPlayerScoreLine().Frames.Count - 1;
                for(var i = 0; i < numberOfFramesToAdd; i++)
                {
                    newScoreLine.AddFrame(new Frame());
                }
            }

            ScoreLines.Add(newScoreLine);
        }

        public PlayerScoreLine GetPlayerScore(string playerName)
        {
            return ScoreLines.FirstOrDefault(sl => sl.PlayerName == playerName);
        }

        public PlayerScoreLine GetCurrentPlayerScoreLine()
        {
            return ScoreLines[_turn];
        }

        public void AddRoll(int pinsKnocked)
        {
            var currentPlayerScoreLine = GetCurrentPlayerScoreLine();

            if (pinsKnocked > NUMBER_OF_PINS)
                throw new InvalidOperationException("Not possible to strike more than 10 pins in one roll");

            if (currentPlayerScoreLine.Frames.Count == 0)
                currentPlayerScoreLine.AddFrame(new Frame());

            currentPlayerScoreLine.AddRoll(pinsKnocked);
            var latestFrame = currentPlayerScoreLine.Frames.Last();
            GameScoreUpdated?.Invoke(this, new GameUpdateEventArgs { ScoreLines = ScoreLines, PlayedFrame = latestFrame });
            
            if (latestFrame.IsClosed)
            {
                _turn = (_turn + 1) % ScoreLines.Count();
                if (_turn == 0)
                {
                    if (latestFrame.IsLastFrame)
                    {
                        EndGame?.Invoke(this, new GameUpdateEventArgs { ScoreLines = ScoreLines, PlayedFrame = latestFrame });
                        return;
                    }
                }
                var nextPlayerScoreLine = GetCurrentPlayerScoreLine();
                nextPlayerScoreLine.AddFrame(new Frame(nextPlayerScoreLine.Frames.Count == (NUMBER_OF_FRAMES - 1)));
            }
            NextRoll?.Invoke(this, new NextRollEventArgs { PlayerToRoll = currentPlayerScoreLine.PlayerName });
        }

        public event EventHandler<NextRollEventArgs> NextRoll;        
        public event EventHandler<GameUpdateEventArgs> GameScoreUpdated;
        public event EventHandler<GameUpdateEventArgs> EndGame;
    }

    public class NextRollEventArgs : EventArgs
    {
        public string PlayerToRoll;
    }

    public class GameUpdateEventArgs : EventArgs
    {
        public List<PlayerScoreLine> ScoreLines { get; set; }
        public Frame PlayedFrame { get; set; }
    }
}
