﻿using BowlingScoreboardBusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace BowlingScoreboardCLI
{
    public class Program
    {

        private static AutoResetEvent _gameWaitHandle;

        static void Main(string[] args)
        {
            using (var _gameWaitHandle = new AutoResetEvent(false))
            {
                var cancelled = false;
                Console.TreatControlCAsInput = false;
                Console.CancelKeyPress += (sender, eventArgs) =>
                {
                    _gameWaitHandle.Set();
                    cancelled = true;
                };

                var scoreboard = new BowlingScoreboard();

                Console.WriteLine("Welcome to this bowling scoreboard. Enter number of players");
                var numberOfPlayers = 0;
                string stringConsoleInput = "";
                while (!cancelled && !int.TryParse((stringConsoleInput = Console.ReadLine()), out numberOfPlayers))
                {
                    if (stringConsoleInput == null)
                    {
                        cancelled = true;
                        break;
                    }
                    Console.WriteLine("Sorry, I didn't get that. Please try again:");
                };
                if (cancelled)
                {
                    Console.WriteLine("Exiting..");
                    return;
                }

                do
                {
                    Console.WriteLine($"Enter the name of player #{scoreboard.ScoreLines.Count + 1}:");
                    var playerName = Console.ReadLine();
                    if (playerName == null)
                    {
                        cancelled = true;
                        break;
                    }
                    if (string.IsNullOrEmpty(playerName))
                    {
                        Console.WriteLine("Sorry, please enter a name. Any name. Only criteria is that it contains at least one character.");
                    }
                    else
                    {
                        try
                        {
                            scoreboard.AddPlayer(playerName);
                        } catch (InvalidOperationException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }
                } while (!cancelled && scoreboard.ScoreLines.Count != numberOfPlayers);
                if (cancelled)
                {
                    Console.WriteLine("Exiting..");
                    return;
                }

                Console.WriteLine("Great! Now we are ready to begin.");

                //Setup game events:
                scoreboard.NextRoll += (sender, eventArgs) =>
                {
                    HandleTurn(scoreboard);
                };
                scoreboard.EndGame += (sender, eventArgs) =>
                {
                    var playerScoreLines = eventArgs.ScoreLines;
                    var maxScore = playerScoreLines.Max(psl => psl.TotalScore);
                    var winners = playerScoreLines.Where(psl => psl.TotalScore == maxScore).Select(psl => psl.PlayerName).ToList();
                    Console.WriteLine("The game is over!");
                    if (winners.Count > 1)
                    {
                        Console.WriteLine("It was a draw between " + string.Join(", ", winners));
                        Console.WriteLine($"They all got {maxScore} points");
                    }
                    else
                    {
                        Console.WriteLine($"{winners.FirstOrDefault()} won with a total of {maxScore} points!");
                    }
                    Console.WriteLine("Press any key to exit.");
                    Console.ReadLine();
                    _gameWaitHandle.Set();
                };
                scoreboard.GameScoreUpdated += (sender, eventArgs) =>
                {
                    Console.WriteLine("Game score updated:");
                    Console.WriteLine(PrettyPrintScoreboard(eventArgs.ScoreLines));

                    var latestPlayedFrame = eventArgs.PlayedFrame;
                    if (latestPlayedFrame.IsGutter)
                    {
                        Console.WriteLine("Oups! A gutter ball. That's embarrassing.");

                    }
                    else if (latestPlayedFrame.IsSpare)
                    {
                        Console.WriteLine("A spare. Good job!");

                    }
                    else if (latestPlayedFrame.IsStrike)
                    {
                        Console.WriteLine("A strike! Great job!");

                    }
                };

                HandleTurn(scoreboard);

                _gameWaitHandle.WaitOne();
                if (cancelled)
                {
                    Console.WriteLine("Exiting..");
                    return;
                }
            }
        }

        private static void HandleTurn(BowlingScoreboard game)
        {
            Console.WriteLine($"{game.GetCurrentPlayerScoreLine().PlayerName}, please enter the number of pins you have knocked down.");
            var roll = 0;
            while (!int.TryParse(Console.ReadLine(), out roll))
            {
                Console.WriteLine("Sorry, I didn't get that. Please try again:");
            };
            try
            {
                game.AddRoll(roll);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine("You tried an invalid action. Maybe you're cheating? Try again.");
                HandleTurn(game);
            }
        }

        private static string PrettyPrintScoreboard(List<PlayerScoreLine> scoreLines)
        {
            var sb = new StringBuilder();
            var longestNameLength = scoreLines.Select(sl => sl.PlayerName).Max(playerName => playerName.Length);

            var begunFrames = scoreLines[0].Frames.Count;
            for (var i = 1; i <= begunFrames; i++)
            {
                if (i == 1)
                {
                    sb.Append("|");
                    sb.Append('\u00A0', longestNameLength + 10);
                    sb.Append("|");
                }
                sb.Append("__" + i + "__|");
            }
            sb.AppendLine();
            foreach (PlayerScoreLine p in scoreLines)
            {
                sb.Append("|");
                sb.Append('\u00A0', longestNameLength + 10);
                sb.Append("|");

                for (var frameIndex = 0; frameIndex < begunFrames; frameIndex++)
                {
                    sb.Append(" ");
                    var currentFrame = p.Frames.ElementAtOrDefault(frameIndex);
                    if (currentFrame != null)
                    {
                        if (currentFrame.Rolls.Count == 0)
                        {
                            sb.Append("-:-");
                        }
                        else if (currentFrame.Rolls.Count == 1)
                        {
                            sb.Append(PinsKnockedToString(currentFrame.Rolls[0], null));
                        }
                        else
                        {
                            sb.Append(PinsKnockedToString(currentFrame.Rolls[0], currentFrame.Rolls[1]));
                        }

                        if (currentFrame.Rolls.Count == 3)
                        {
                            var lastRoll = currentFrame.Rolls[2];
                            if (lastRoll == 10)
                            {
                                sb.Append(" X");
                            } else
                            {
                                sb.Append(" " + lastRoll.ToString());
                            }
                        }
                    }
                    else
                    {
                        sb.Append("-:-");
                    }
                    sb.Append(" |");
                }

                sb.AppendLine();
                sb.Append("|");
                sb.Append(p.PlayerName);
                sb.Append('\u00A0', ((longestNameLength + 10) - p.PlayerName.Length));
                sb.Append("|");

                for (var i = 0; i < begunFrames; i++)
                {
                    sb.Append(" ");
                    if (p.Frames.ElementAtOrDefault(i) != null)
                    {
                        var totalScore = 0;
                        for (var j = 0; j <= i; j++)
                        {
                            totalScore += p.Frames[j].TotalScore;
                        }
                        var totalScoreAsString = totalScore.ToString();
                        sb.Append(totalScoreAsString);
                        sb.Append(' ', 3 - totalScoreAsString.Length);
                    }
                    else
                    {
                        sb.Append("0  ");
                    }
                    sb.Append(" |");
                }
                sb.AppendLine();

            }
            return sb.ToString();
        }

        private static string PinsKnockedToString(int firstRoll, int? secondRoll)
        {
            var result = "";
            if (firstRoll == 10)
            {
                result += "X";
            } else
            {
                result += firstRoll.ToString();
            }
            result += ":";

            if (secondRoll == null)
            {
                result += "-";
            } else if (secondRoll == 10)
            {
                result += "X";
            }
            else if ((firstRoll + secondRoll) == 10)
            {
                result += "/";
            }
            else result += secondRoll.ToString();

            return result;
        }

    }
}
